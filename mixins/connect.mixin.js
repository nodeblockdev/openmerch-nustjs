export default {

  data() {
    return {
      apiService: null
    }
  },

  computed: {
    getCollections() {
      if (this.isConnected)
        return this.$store.getters["wallet/getCollections"]
      this.collection = "";
      return [];
    },
    getChainId() {
      // if (this.isConnected)
        return this.$store.getters["wallet/getChainId"];
      // return 0
    },
    isConnected() {
      return this.$store.getters["wallet/isConnected"] && this.$auth.loggedIn;
    },
    walletAddress() {
      return this.$store.getters["wallet/getWalletAddress"];
    },
    walletAddressBriefed() {
      return this.$store.getters["wallet/getWalletAddressBriefed"];
    },
    profileImage() {
      return this.$store.getters['wallet/getProfileImage']
    },

  },

  methods: {

  },
}
