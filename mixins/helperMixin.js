import {Helper} from "~/helpers/helper";

export default {
  computed: {
    isUpgrading() {
      return Helper.isUpgrading()
    },
    UpgradingText() {
      return Helper.getUpgradingText()
    }
  }
}
