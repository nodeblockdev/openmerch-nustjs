export class Helper {
  static isDebug() {
    return process.env.DEBUG === "true";
  }

  static isUpgrading() {
    return process.env.UPGRADING === "true";
  }

  static getUpgradingText() {
    return process.env.UPGRADING_TEXT;
  }

  static getTestWallet() {
    if (process.env.TEST_WALLET) return process.env.TEST_WALLET;
    return "0x4ba84d2Ca525AA578f8e097ACD4141DFD52B3E74";
  }
}
