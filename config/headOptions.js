export default {
  title: "OpenMerch | Landing NFTs on the physical world",
  meta: [
    { charset: "utf-8" },
    {
      name: "viewport",
      content:
        "width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no",
    },
    {
      hid: "description",
      name: "description",
      content:
        "The world’s first marketplace to combine crypto-collectibles and non-fungible tokens (NFTs) with physical products. Buy, sell, and discover exclusive merchandise powered by digital items.",
    },
    {
      name: "format-detection",
      content: "telephone=no",
    },
    {
      name: "msapplication-TileColor",
      content: "#da532c",
    },
    {
      name: "theme-color",
      content: "#ffffff",
    },
    {
      name: "twitter:card",
      content: "summary",
    },
    {
      name: "twitter:site",
      content: "@ui8",
    },
    {
      name: "twitter:title",
      content: "OpenMerch | Landing NFTs on the physical world",
    },
    {
      name: "twitter:description",
      content: "Summon your favorite NFT on physical products",
    },
    {
      name: "twitter:social",
      content: "https://openMerch.io/img/social/card.png",
    },
    {
      name: "og:title",
      content: "OpenMerch | Landing NFTs on the physical world",
    },
    {
      name: "og:type",
      content: "website",
    },
    {
      name: "og:url",
      content: "https://openmerch.io",
    },
    {
      name: "og:image",
      content: "https://openMerch.io/img/social/card.png",
    },
    {
      name: "og:social",
      content: "https://openMerch.io/img/social/card.png",
    },
    {
      name: "og:description",
      content: "Summon your favorite NFT on physical products",
    },
    {
      name: "og:site_name",
      content: "OpenMerch",
    },
    {
      name: "fb:admins",
      content: "132951670226590",
    },
  ],
  link: [
    { rel: "icon", type: "social/x-icon", href: "/favicon.ico" },

    {
      rel: "apple-touch-icon",
      href: "/img/apple-touch-icon.png",
    },
    { rel: "icon", sizes: "57x57", href: "/img/favi-57.png" },
    { rel: "icon", sizes: "32x32", href: "/img/favi-32.png" },
    { rel: "icon", sizes: "16x16", href: "/img/favi-16.png" },
    { rel: "manifestmanifest", href: "/img/site.webmanifest" },
    {
      rel: "mask-icon",
      color: "#5bbad5",
      href: "/img/safari-pinned-tab.svg",
    },
    { rel: "preconnect", href: "https://fonts.gstatic.com" },
    {
      rel: "stylesheet",
      href: "https://fonts.googleapis.com/css2?family=DM+Sans:wght@700&amp;family=Poppins:wght@400;500;600;700&amp;display=swap",
    },
    {
      rel: "stylesheet",
      href: "/assets/feather-icons-web/feather.css",
    },
  ],
  script: [
    //
    // {
    //   src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
    //   type: "text/javascript"
    // },
    { src: "/js/lib/jquery.min.js", type: "text/javascript", body: true },
    {
      src: "/js/lib/jquery.countdown.min.js",
      type: "text/javascript",
      body: true,
    },
    {
      src: "/js/lib/jquery.magnific-popup.min.js",
      type: "text/javascript",
      body: true,
    },
    {
      src: "/js/lib/jquery.nice-select.min.js",
      type: "text/javascript",
      body: true,
    },
    { src: "/js/lib/nouislider.min.js", type: "text/javascript", body: true },
    { src: "/js/lib/share-buttons.js", type: "text/javascript", body: true },
    { src: "/js/lib/slick.min.js", type: "text/javascript", body: true },
    { src: "/js/lib/wNumb.js", type: "text/javascript", body: true },
    // { hid: 'web3', src: 'https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js', body: true },
    // { hid: 'moralis', src: 'https://unpkg.com/moralis/dist/moralis.js', body: true },
    // {type: "text/javascript", src: '/js/demo.js', type: "text/javascript", body: true},
  ],
};
