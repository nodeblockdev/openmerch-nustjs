export default {
  AUTH_LOGIN: "/auth/login",
  AUTH_LOGOUT: "/auth/logout",
  AUTH_PROFILE: "/auth/profile",

  UPDATE_PROFILE: "/users",
  UPDATE_PROFILE_COVER: "/users/cover",
  GET_PROFILE_COVER: "/users/cover",
  GET_BALANCE: "/users/balance",

  SETTING_CATEGORIES: "/setting/categories",
  CATEGORIES: "/categories",
  PAYOUT_STORE: "/payout",
  NFT_COLLECTIONS: "/nfts",
  ASSETS: "/assets",
  MERCHS: "/merchs",

  FIND_ASSET_BY_TOKEN: "/assets/:token/token",
};
