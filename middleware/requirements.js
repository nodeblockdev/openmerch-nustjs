import apiRoutes from "~/config/apiRoutes";
import { UPDATE_CATEGORIES } from "~/store/categories";
import { INITIATION_ETHEREUM, UPDATE_BALANCE } from "~/store/wallet";
import { CollectionService } from "~/services/collectionService";
import { UPDATE_ASSETS, UPDATE_BASE_COLLECTION } from "~/store/collections";

export default async function ({ $auth, $axios, store, $cookies, redirect }) {
  if ($auth.loggedIn) {
    await $axios.$get(apiRoutes.CATEGORIES).then((data) => {
      store.dispatch("categories/" + UPDATE_CATEGORIES, data);
    });

    if ($cookies.get("wallet-connected")) {
      await store
        .dispatch("wallet/" + INITIATION_ETHEREUM)
        .then(() => {
          $cookies.set("wallet-connected", true, {
            path: "/",
            maxAge: 60 * 60 * 24 * 7,
          });
        })
        .then(async () => {
          await $axios.$get("/setting/collections").then((data) => {
            store.dispatch("collections/" + UPDATE_BASE_COLLECTION, data);
          });
          let contracts = store.getters["collections/base"].map((item) => {
            return item.address;
          });
          const walletAssets = await CollectionService.getWalletAssets(
            store.getters["wallet/getWalletAddress"],
            contracts
          );
          await store.dispatch("collections/" + UPDATE_ASSETS, walletAssets);

          await $axios
            .$put(apiRoutes.GET_BALANCE, {
              tokens: walletAssets?.map((item) => {
                return item.token_id;
              }),
            })
            .then((data) => {
              store.dispatch("wallet/" + UPDATE_BALANCE, data);
            });
        })
        .catch((err) => {
          $cookies.remove("wallet-connected");
          $auth.logout();
          alert("Metamask not installed");
          return redirect("/connect");
        });
    }
  }
}
