if (localStorage.getItem('darkMode') === "on") {
  document.body.classList.add("dark");
  document.addEventListener("DOMContentLoaded", function () {
    document.querySelector('.js-theme input').checked = true;
  });
}
