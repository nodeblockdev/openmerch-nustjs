import axios from "axios";

export class ApiProvider {
  BASE = "https://api.opensea.io/api/v1";

  async getWalletCollections(walletAddress) {
    return new Promise((resolve, reject) => {
      const options = { method: "GET" };
    });
  }

  async getWalletAssets() {
    return await axios
      .get(`${process.env.BASE_URL}/auth/profile?page=0&limit=10000`, {
        headers: {
          authorization: localStorage.getItem("auth._token.local"),
        },
      })
      .then(async (response) => {
        this.NftsUpdateStatus = response.data.user.assetsUpdateStatus;
        if (this.NftsUpdateStatus == "updating") {
          setTimeout(() => this.fetchAssets(), 2000);
        }
        return response.data.assets;
      });
  }
}
