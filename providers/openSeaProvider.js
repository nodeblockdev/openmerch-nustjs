import { HttpService } from "~/services/httpService";
import wallet from "~/store/wallet";

export class OpenSeaProvider {
  BASE = "https://api.opensea.io/api/v1";

  async getWalletCollections(walletAddress) {
    return new Promise((resolve, reject) => {
      const options = { method: "GET" };
      // fetch(
      //   this.BASE +
      //     "/collections?asset_owner=" +
      //     walletAddress +
      //     "&offset=0&limit=20",
      //   options
      // )
      //   .then((response) => {
      //     console.log(response.json());
      //   })
      //   .catch((response) => reject(response));
    });
  }

  async getWalletAssets(walletAddress, asset_contract_addresses) {
    return new Promise((resolve, reject) => {
      const options = { method: "GET" };
      try {
        fetch(
          this.BASE +
          "/assets?" +
          HttpService.buildQueryNoneArray({
            owner: walletAddress,
            asset_contract_addresses: asset_contract_addresses,
            order_direction: "desc",
            offset: 0,
            limit: 20,
          }),
          options
        )
          .then(async (response) => {
            let data = await response.json();
            try {
              data = data.assets.map((item) => {
                return {
                  name: item.name,
                  collection: item.collection.name,
                  slug: item.collection.slug,
                  image_url: item.image_url,
                  thumbnail: item.image_thumbnail_url,
                  address: item.asset_contract.address,
                };
              });
              resolve(data);
            } catch (e) {
              console.log(e);
              resolve([]);
            }
          })
          .catch((response) => reject(response));
      } catch (e) {
        console.log(e);
      }
    });
  }
}
