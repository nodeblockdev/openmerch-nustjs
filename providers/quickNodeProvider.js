const ethers = require("ethers");

export class QuickNodeProvider {
  constructor() {
    this.provider = new ethers.providers.JsonRpcProvider(
      process.env.QUICK_NODE_API
    );
  }

  async getWalletAssets(walletAddress, asset_contract_addresses) {
    try {
      const heads = await this.provider.send("qn_fetchNFTs", [
        walletAddress,
        [...asset_contract_addresses],
      ]);
      try {
        return heads.assets.map((item) => {
          return {
            name: item.name,
            collection: item.collectionName,
            slug: item.collectionAddress,
            image_url: item.image,
            thumbnail: item.image,
            address: item.collectionAddress,
          };
        });
      } catch (e) {
        return [];
      }
    } catch (e) {
      console.log(e);
    }
  }
}
