import apiRoutes from "./config/apiRoutes";
import headOptions from "./config/headOptions";

export default {
  ssr: false,

  loading: "~/components/Loading.vue",

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: headOptions,

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/css/app.min.css",
    "~/assets/css/app.css",
    "~/assets/css/keyframe.css",
    "~/assets/css/style.css",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/axios",
    "~/plugins/vuelidate",
    "~/plugins/debounce",
    "~/plugins/sse",
    "~/plugins/voutside",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["@nuxtjs/dotenv"],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    "@nuxtjs/auth-next",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    "cookie-universal-nuxt",
    "@nuxtjs/axios",
    "@nuxtjs/dotenv",
    "@nuxtjs/toast",
    "@nuxtjs/recaptcha",
    "nuxt-vue-select",
    "bootstrap-vue/nuxt",
    [
      "nuxt-lazy-load",
      {
        // Your options
        // images: true,
        // videos: true,
        // audios: true,
        // iframes: true,
        // native: false,
        directiveOnly: true,

        // Default image must be in the static folder
        // defaultImage: '~/assets/images/loading.gif',

        // To remove class set value to false
        loadingClass: "isLoading",
        loadedClass: "isLoaded",
        appendClass: "lazyLoad",

        observerConfig: {
          // See IntersectionObserver documentation
        },
      },
    ],
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_URL || "http://localhost:8000",
    // crossDomain: false,
    // crossOrigin: false,
    // proxyHeaders: false,
    // credentials: false,
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      /* meta options */
      favicon: false,
    },
    icon: {
      /* meta options */
      source: "/favicon.png",
    },
    manifest: {
      lang: "en",
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  toast: {
    position: "bottom-right",
    duration: 3000,
  },

  recaptcha: {
    hideBadge: true, // Hide badge element (v3 & v2 via size=invisible)
    language: "en", // Recaptcha language (v2)
    version: 1, // Version
    size: "invisible", // Size: 'compact', 'normal', 'invisible' (v2)
  },
  publicRuntimeConfig: {
    recaptcha: {
      /* reCAPTCHA options */
      siteKey: process.env.RECAPTCHA_SITE_KEY,
    },
  },

  auth: {
    redirect: {
      login: "/connect",
      logout: "/connect",
      callback: "/connect",
      home: false,
    },
    strategies: {
      local: {
        token: {
          property: "access_token",
          // global: true,
          // required: true,
          // type: 'Bearer'
        },
        user: {
          property: "user",
          autoFetch: true,
        },
        endpoints: {
          login: { url: apiRoutes.AUTH_LOGIN, method: "post" },
          logout: { url: apiRoutes.AUTH_LOGOUT, method: "post" },
          user: { url: apiRoutes.AUTH_PROFILE, method: "get" },
        },
      },
    },
  },
  env: {
    baseURL: process.env.baseURL,
    CDN_URL: process.env.CDN_URL,
    BASE_MEDIA: process.env.BASE_MEDIA,
    APP_PROVIDER: process.env.APP_PROVIDER,
    DEBUG: process.env.DEBUG,
    QUICK_NODE_API: process.env.QUICK_NODE_API,
    TestWallet: process.env.TestWallet,
    RECAPTCHA_SITE_KEY: process.env.RECAPTCHA_SITE_KEY,
    MORALIS_SERVER_URL: process.env.MORALIS_SERVER_URL,
    MORALIS_API_KEY: process.env.MORALIS_API_KEY,
    MORALIS_APP_ID: process.env.MORALIS_APP_ID,
    CDN_URL: process.env.CDN_URL,
    MORALIS_WEB_API_KEY: process.env.MORALIS_WEB_API_KEY,
    Access_Control_Allow_Origin: process.env.Access_Control_Allow_Origin,
  },
};
