let httpBuildQuery = require('http-build-query');

export class HttpService {
  static buildQuery(object) {
    return httpBuildQuery(object);
  }

  static buildQueryNoneArray(object) {
    let query = [];
    Object.keys(object).forEach((key) => {
      if (Array.isArray(object[key])) {
        object[key].forEach(item2 => {
          query.push(key + '=' + item2)
        })
      } else {
        query.push(key + '=' + object[key])
      }
    });
    return query.join('&');
  }
}
