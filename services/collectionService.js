import { OpenSeaProvider } from "~/providers/openSeaProvider";
import { QuickNodeProvider } from "~/providers/quickNodeProvider";
import { ApiProvider } from "~/providers/apiProvider";
class iCollectionService {
  /*
  wallet => the user wallet address
  provider => is specified that use OpenSea Api or QuickNode nft tool
  contracts => list of contracts address
   */
  constructor() {
    switch (process.env.APP_PROVIDER) {
      case "OpenSea":
        this.provider = new OpenSeaProvider();
        break;
      case "QuickNode":
        this.provider = new QuickNodeProvider();
        break;

      case "Server":
        this.provider = new ApiProvider();
        break;
    }
  }

  async getWalletAssets(wallet, contracts) {
    if (wallet && contracts)
      return await this.provider.getWalletAssets(wallet, contracts);
  }
}

export const CollectionService = new iCollectionService();
