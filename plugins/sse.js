// OR specify custom defaults (described below)
import Vue from "vue";
import VueSSE from 'vue-sse';

export default function () {
  Vue.use(VueSSE, {
    format: 'json',
    polyfill: true,
    url: process.env.BASE_URL + '/orders/sse',
    withCredentials: false,
  });

};
