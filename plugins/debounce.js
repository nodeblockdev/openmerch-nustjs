import Vue from 'vue'
import vueDebounce from 'vue-debounce'

export default function () {
  Vue.use(vueDebounce, {
    listenTo: ['input', 'keyup'],
    defaultTime: '700ms'
  });
}

