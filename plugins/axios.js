export default function ({$ctx, $axios, redirect}) {
  $axios.onRequest((config) => {
    // console.log('Making request to ' + config.url)
  });

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status);
    if (code === 404) {
      $ctx.error({ statusCode: 404, message: 'Post not found' })
      // redirect("/404");
    }
  })

  $axios.setHeader('x-api-key', Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5));
  $axios.setHeader('Access-Control-Allow-Origin', '*');
  $axios.setHeader('Cache-Control', 'no-cache');
}
