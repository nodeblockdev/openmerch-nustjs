// actions
export const CHECKOUT_UPDATE_ORDER = 'CHECKOUT_UPDATE_ORDER'

// mutations
export const CHECKOUT_SET_ORDER = 'CHECKOUT_SET_ORDER'


export default {
  name: 'order',
  namespaced: true,
  state() {
    return {
      order: {}
    }
  },
  getters: {
    order: state => {
      return state.order
    },
  },
  actions: {
    [CHECKOUT_UPDATE_ORDER](context, payload) {
      context.commit(CHECKOUT_SET_ORDER, payload);
    },
  },

  mutations: {
    [CHECKOUT_SET_ORDER](state, payload) {
      state.order= payload
    },
  },
};
