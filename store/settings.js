// actions
export const UPDATE_SETTINGS = 'UPDATE_SETTINGS'

// mutations
export const SET_SETTINGS = 'SET_SETTINGS'

export default {
  name: 'wallet',
  namespaced: true,
  state() {
    return {
      options: {},
    }
  },
  getters: {
    getOptions: state => {
      return state.options
    },
  },
  actions: {
    [UPDATE_SETTINGS](context, payload) {
      context.commit(SET_SETTINGS, payload);
    }
  },

  mutations: {
    [SET_SETTINGS](state, payload) {
      state.options = payload;
    }
  },
};
