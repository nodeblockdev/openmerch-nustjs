// actions
export const UPDATE_NFTS = "UPDATE_NFTS";
export const RESET_ASSETS = "RESET_ASSETS";
// mutations
export const SET_NFTS = "SET_NFTS";

export default {
  name: "nfts",
  namespaced: true,
  state() {
    return {
      nfts: [],
      attributes: [],
    };
  },
  getters: {
    all: (state) => {
      console.log(
        "%cnftCollections.js line:18 state.nfts",
        "color: #007acc;",
        state.nfts.items
      );
      return state.nfts;
    },
    attributes: (state) => {
      return state.attributes;
    },
  },
  actions: {
    [UPDATE_NFTS](context, payload) {
      context.commit(SET_NFTS, payload);
    },
    [RESET_ASSETS](context, payload) {
      context.commit(RESET_ASSETS);
    },
  },

  mutations: {
    [SET_NFTS](state, payload) {
      state.nfts = payload;
    },
    [RESET_ASSETS](state, payload) {
      state.nfts.assets = [];
    },
  },
};
