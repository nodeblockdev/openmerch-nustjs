export const UPDATE_ATTRIBUTES = "UPDATE_ATTRIBUTES";
export const SET_ATTRIBUTES = "SET_ATTRIBUTES";

export default {
  name: "attributes",
  namespaced: true,
  state() {
    return {
      attributes: [],
    };
  },
  getters: {
    all: (state) => {
      return state.attributes;
    },
  },
  actions: {
    [UPDATE_ATTRIBUTES](context, payload) {
      context.commit(SET_ATTRIBUTES, payload);
    },
  },

  mutations: {
    [SET_ATTRIBUTES](state, payload) {
      state.attributes = payload;
    },
  },
};
