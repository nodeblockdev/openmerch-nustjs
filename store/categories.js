// actions
export const UPDATE_CATEGORIES = "UPDATE_CATEGORIES";

// mutations
export const SET_CATEGORIES = "SET_CATEGORIES";

export default {
  name: "categories",
  namespaced: true,
  state() {
    return {
      categories: [],
    };
  },
  getters: {
    all: (state) => {
      return state.categories;
    },
  },
  actions: {
    [UPDATE_CATEGORIES](context, payload) {
      context.commit(SET_CATEGORIES, payload);
    },
  },

  mutations: {
    [SET_CATEGORIES](state, payload) {
      state.categories = payload;
    },
  },
};
