// actions
export const UPDATE_ORDER = "UPDATE_ORDER";
export const RESET_ORDER = "RESET_ORDER";

// mutations
export const SET_ORDER = "SET_ORDER";
export const CLEAR_ORDER = "CLEAR_ORDER";

export default {
  name: "order",
  namespaced: true,
  state() {
    return {
      countries: [],
      count: 1,
      merches: [],
      background: "",
      asset: "",
      ETH: 0,
      shipping: 0,
      purchaseStep: 1,
      showPurchaseModal: false,
      checkoutDetails: {},
    };
  },
  getters: {
    base: (state) => {
      return state;
    },
    orderCount: (state) => {
      return state.count;
    },
    selectedMerchForOrder: (state) => {
      return state.merches;
    },
  },
  actions: {
    [UPDATE_ORDER](context, payload) {
      context.commit(SET_ORDER, payload);
    },
    [RESET_ORDER](context, payload) {
      context.commit(CLEAR_ORDER, payload);
    },
  },

  mutations: {
    [SET_ORDER](state, payload) {
      state[payload.prop] = payload.value;
    },
    [CLEAR_ORDER](state, payload) {
      state[payload.prop] = payload.value;
    },
  },
};
