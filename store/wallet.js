// actions
export const INITIATION_ETHEREUM = "INITIATION_ETHEREUM";
export const WALLET_LOGOUT = "WALLET_LOGOUT";
export const UPDATE_WALLET_ADDRESS = "UPDATE_WALLET_ADDRESS";
export const UPDATE_BALANCE = "UPDATE_BALANCE";
export const UPDATE_STATUS = "UPDATE_STATUS";
export const UPDATE_PAYOUT = "UPDATE_PAYOUT";

// mutations
export const SET_LOGOUT = "SET_LOGOUT";
export const SET_CHAIN_ID = "SET_CHAIN_ID";
export const SET_BALANCE = "SET_BALANCE";
export const SET_STATUS = "SET_STATUS";
export const SET_PAYOUT = "SET_PAYOUT";

export default {
  name: "wallet",
  namespaced: true,
  state() {
    return {
      status: "",
      wallet: "",
      chainId: 0,
      balance: 0,
      payout: 0,
      total_sales: 0,
    };
  },

  getters: {
    getChainId: (state) => {
      return state.chainId;
    },
    getWalletAddress: (state) => {
      if (process.env.DEBUG === "true") return process.env.TestWallet;
      return state.wallet ? state.wallet : "";
    },
    getWalletAddressBriefed: (state) => {
      if (state.wallet)
        return (
          state.wallet.toString().substring(0, 5) +
          "..." +
          state.wallet
            .toString()
            .substring(
              state.wallet.toString().length,
              state.wallet.toString().length - 8
            )
        );
      return "";
    },
    isConnected: (state) => {
      try {
        if (typeof window.ethereum !== "undefined") {
          return window.ethereum.isConnected && state.wallet.length > 0;
        }
        return false;
      } catch (e) {
        return false;
      }
    },
    balance: (state) => {
      return state.balance;
    },
    payout: (state) => {
      return state.payout;
    },
    total_sales: (state) => {
      return state.total_sales;
    },
    status: (state) => {
      return state.status;
    },
  },

  actions: {
    [INITIATION_ETHEREUM](context, payload) {
      return new Promise((resolve, reject) => {
        if (typeof window.ethereum !== "undefined") {
          const ethereum = window.ethereum;
          ethereum.on("accountsChanged", function (accounts) {
            // Time to reload your interface with accounts[0]!
            context.commit(UPDATE_WALLET_ADDRESS, accounts[0]);
          });
          ethereum.on("chainChanged", async (value) => {
            await context.commit(SET_CHAIN_ID, value);
          });
          window.ethereum.enable();
          if (ethereum.isConnected) {
            console.log(
              "%cwallet.js line:92  process.env.TestWallet",
              "color: #007acc;",
              process.env.DEBUG
            );
            if (process.env.DEBUG == "true") {
              context.commit(UPDATE_WALLET_ADDRESS, process.env.TestWallet);
            } else {
              context.commit(UPDATE_WALLET_ADDRESS, ethereum.selectedAddress);
            }
            context.commit(SET_CHAIN_ID, window.ethereum.chainId);
            console.log(
              "%cwallet.js line:101 process.env.DEBUG ",
              "color: #007acc;",
              process.env.DEBUG
            );
            if (process.env.DEBUG == "true") resolve(process.env.TestWallet);
            else resolve(ethereum.selectedAddress);
          } else {
            ethereum.request({ method: "eth_requestAccounts" });
          }
        } else {
          reject();
        }
      });
    },
    [WALLET_LOGOUT](context, payload) {
      context.commit(SET_LOGOUT, payload);
    },
    [UPDATE_BALANCE](context, payload) {
      context.commit(SET_BALANCE, payload);
    },
    [UPDATE_PAYOUT](context, payload) {
      context.commit(SET_PAYOUT, payload);
    },
    [UPDATE_STATUS](context, payload) {
      context.commit(SET_STATUS, payload);
    },
  },

  mutations: {
    [SET_CHAIN_ID](state, payload) {
      console.log("The network has been changed");
      state.chainId = payload;
    },
    [UPDATE_WALLET_ADDRESS](state, payload) {
      state.wallet = payload;
    },
    [SET_LOGOUT](state, payload) {
      state.wallet = "";
    },
    [SET_BALANCE](state, payload) {
      state.balance = payload.balance;
      state.total_sales = payload.totalSales;
    },
    [SET_PAYOUT](state, payload) {
      state.payout = payload.payout;
    },
    [SET_STATUS](state, payload) {
      state.status = payload.status;
    },
  },
};
