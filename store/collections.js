// actions
export const UPDATE_COLLECTION = "UPDATE_COLLECTION";
export const UPDATE_BASE_COLLECTION = "UPDATE_BASE_COLLECTION";
export const UPDATE_ASSETS = "UPDATE_ASSETS";

// mutations
export const SET_COLLECTION = "SET_COLLECTION";
export const SET_ASSETS = "SET_ASSETS";
export const RESET_ASSETS = "RESET_ASSETS";
export const SET_AVATAR = "SET_AVATAR";
export const SET_BASE_COLLECTION = "SET_BASE_COLLECTION";

export default {
  name: "wallet",
  namespaced: true,
  state() {
    return {
      avatar: "/img/content/def.jpg",
      assets: [],
      collections: [],
      base_collections: [],
      base_addresses: [],
    };
  },
  getters: {
    base: (state) => {
      return state.base_collections;
    },
    user: (state) => {
      return state.collections;
    },
    assets: (state) => {
      return state.assets;
    },
    avatar: (state) => {
      return state.avatar;
    },
  },
  actions: {
    [UPDATE_COLLECTION](context, payload) {
      context.commit(SET_COLLECTION, payload);
    },
    async [UPDATE_BASE_COLLECTION](context, payload) {
      context.commit(SET_BASE_COLLECTION, payload);
      context.commit(RESET_ASSETS);
    },
    async [UPDATE_ASSETS](context, payload) {
      context.commit(SET_ASSETS, payload);
      context.commit(SET_AVATAR);
      // context.commit(RESET_ASSETS);
    },
  },

  mutations: {
    [SET_AVATAR](state) {
      if (state.assets.length > 0) state.avatar = state.assets[0].image;
      else state.avatar = "/img/content/def.jpg";
    },
    [SET_ASSETS](state, payload) {
      state.assets = payload;
    },
    [RESET_ASSETS](state) {
      let items = [];
      if (state.assets)
        state.assets.forEach((item, index) => {
          if (state.base_addresses.indexOf(item.address) > -1) items.push(item);
        });
      state.assets = items;
    },
    [SET_COLLECTION](state, payload) {
      state.collections = payload;
    },
    [SET_BASE_COLLECTION](state, payload) {
      state.base_collections = payload;
      state.base_addresses = payload.map((item) => {
        return item.address;
      });
    },
  },
};
